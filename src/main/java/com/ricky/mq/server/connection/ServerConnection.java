package com.ricky.mq.server.connection;

import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.ricky.mq.server.code.constant.MessageTemplate;
import com.ricky.mq.server.code.handler.ClientDataHandler;
import com.ricky.mq.server.code.manager.QueueManager;
import com.ricky.mq.server.code.manager.TopicManager;
import com.ricky.mq.server.code.message.data.BaseData;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * @author Ricky
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ServerConnection implements Runnable {
    /**
     * 写日志的
     */
    private static Log LOG = LogFactory.get(ServerConnection.class);
    /**
     * 本身
     */
    private static ServerConnection serverConnection = new ServerConnection();
    /**
     * 连接通道
     */
    private ServerSocketChannel serverSocketChannel;
    /**
     * 轮训器
     */
    private Selector selector;
    /**
     * 读buff
     */
    private ByteBuffer readBuf = ByteBuffer.allocate(0xffff);
    /**
     * 写buff
     */
    private ByteBuffer writeBuf;
    /**
     * 服务器端口
     */
    private int port = 2428;


    /**
     * 获取本身
     *
     * @return ServerConnection
     */
    public static ServerConnection getServerConnection() {
        return serverConnection;
    }


    /**
     * 设置服务器端口，默认2428
     *
     * @param port
     * @return
     */
    public ServerConnection port(int port) {
        this.port = port;
        return this;
    }

    /**
     * 设置读buff大小，默认65535
     *
     * @param readBufSize
     * @return ClientConnection
     */
    public ServerConnection readBufSize(int readBufSize) {
        readBuf = ByteBuffer.allocate(readBufSize);
        return this;
    }

    /**
     * 启动服务
     *
     * @return ServerConnection
     */
    public ServerConnection start() {
        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
            selector = Selector.open();
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            start0();
            LOG.debug("BoringMQ-Server 已启动, 端口：" + port);
        } catch (IOException e) {
            throw new RuntimeException("BoringMQ-Server 启动失败", e);
        }
        return this;
    }

    /**
     * 启动服务端
     *
     * @return
     */
    public void start0() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        try {
            while (true) {
                selector.select();
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    switch (key.readyOps()) {
                        // 连接接收事件，表示服务器监听到了客户连接，服务器可以接收这个连接~
                        case SelectionKey.OP_ACCEPT:
                            ServerSocketChannel acceptChannel = (ServerSocketChannel) key.channel();
                            SocketChannel socketChannel = acceptChannel.accept();
                            socketChannel.configureBlocking(false);
                            socketChannel.register(selector, SelectionKey.OP_READ);

                            InetSocketAddress remoteAddress = (InetSocketAddress) socketChannel.getRemoteAddress();
                            LOG.info("客户端连接成功，客户端地址：" + remoteAddress.getAddress().getHostAddress());
                            break;
                        // 读事件
                        case SelectionKey.OP_READ:
                            SocketChannel readChannel = (SocketChannel) key.channel();
                            readBuf.clear();
                            int size = 0;
                            try {
                                size = readChannel.read(readBuf);
                            } catch (IOException e) {
                                closeSocket(readChannel);
                                break;
                            }
                            if (size >= 0) {
                                readBuf.flip();
                                byte[] array = new byte[readBuf.remaining()];
                                readBuf.get(array);
                                ClientDataHandler.handler(readChannel, new String(array));
                                readBuf.clear();
                            } else {
                                closeSocket(readChannel);
                            }
                            break;
                        default:
                            break;
                    }
                    iterator.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送处理好的数据给客户端
     *
     * @param socketChannel
     * @param callbackData
     */
    public void send(SocketChannel socketChannel, String commandCode, BaseData callbackData) {
        String assemblyData = MessageTemplate
                .build()
                .setCommandCode(commandCode)
                .setData(HexUtil.encodeHexStr(JSONUtil.parseObj(callbackData).toString()))
                .assemblyData();

        try {
            if (socketChannel.isOpen()) {
                writeBuf = ByteBuffer.wrap(assemblyData.getBytes());
                socketChannel.write(writeBuf);
                writeBuf.clear();
                socketChannel.register(selector, SelectionKey.OP_READ);
            } else {
                closeSocket(socketChannel);
            }
        } catch (IOException e) {
            LOG.error("发送消息失败！", e);
        }
    }

    /**
     * 关闭连接
     * @param socketChannel
     */
    public void closeSocket(SocketChannel socketChannel) {
        try {
            if (ObjectUtil.isNotEmpty(socketChannel) && socketChannel.isOpen()) {
                InetSocketAddress remoteAddress = (InetSocketAddress) socketChannel.getRemoteAddress();
                LOG.info("客户端已断开连接！" + remoteAddress.getAddress().getHostAddress());
                socketChannel.socket().close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            QueueManager.removerQueueForChannel(socketChannel);
            TopicManager.removerTopicForChannel(socketChannel);
        }
    }
}

