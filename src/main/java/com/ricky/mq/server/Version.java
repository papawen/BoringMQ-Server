package com.ricky.mq.server;

/**
 * 当前版本
 *
 * @author ricky
 * @since 2020.06.18
 */
public class Version {
    /**
     * 实际的版本号
     */
    public static final String VERSION = "1.0";
    /**
     * 报文中的版本号
     */
    public static final String MESSAGE_VERSION = "0001";

}
