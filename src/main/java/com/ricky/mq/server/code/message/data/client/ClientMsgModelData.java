package com.ricky.mq.server.code.message.data.client;

import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 客户端消息模型对象
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ClientMsgModelData extends BaseData {

    /**
     * 消息模型名
     */
    private String modelMsgName;
    /**
     * 最大容量
     */
    private int maxSize;

    public static ClientMsgModelData build() {
        return new ClientMsgModelData();
    }

    public String getModelMsgName() {
        return modelMsgName;
    }

    public ClientMsgModelData setModelMsgName(String modelMsgName) {
        this.modelMsgName = modelMsgName;
        return this;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public ClientMsgModelData setMaxSize(int maxSize) {
        this.maxSize = maxSize;
        return this;
    }
}
