package com.ricky.mq.server.code.manager;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ricky.mq.server.code.message.BoringQueueMsg;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 队列管理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class QueueManager {
    /**
     * 监听器锁，为了防止服务器未启动时，客户端先监听队列时找不到队列而报错
     * 加了锁以后可以同步等待创建队列
     */
    private static Lock listenerLock = new ReentrantLock();
    /**
     * 报保存所有监听器锁条件
     */
    private static Map<String, Condition> listenerConditionMap = new ConcurrentHashMap<>(16);
    /**
     * 消息锁，为了防止服务器未启动时，客户端先发送队列消息时找不到队列而报错
     * 加了锁以后可以同步等待创建队列
     */
    private static Lock msgLock = new ReentrantLock();
    /**
     * 报保存所有监听器锁条件
     */
    private static Map<String, Condition> msgConditionMap = new ConcurrentHashMap<>(16);
    /**
     * 保存所有的queue
     */
    private static ArrayList<BoringQueueMsg> boringQueueMsgList = new ArrayList<>(16);
    /**
     * 保存所有的queueName与 Queue对应关系
     */
    private static Map<String, BoringQueueMsg> boringQueueMsgMap = new ConcurrentHashMap<>(16);
    /**
     * 保存每个queue所关联的管道
     */
    private static Map<String, List<SocketChannel>> queueForChannels = new ConcurrentHashMap<>(16);

    /**
     * 添加一个BoringQueueMsg
     *
     * @param queueMsg 队列消息
     */
    public static void addBoringQueueMsg(BoringQueueMsg queueMsg) {
        if (!boringQueueMsgMap.containsKey(queueMsg.getQueueName())) {
            boringQueueMsgMap.put(queueMsg.getQueueName(), queueMsg);
            boringQueueMsgList.add(queueMsg);
        }

        msgLock.lock();
        try {
            msgConditionMap.get(queueMsg.getQueueName()).signal();
        } catch (Exception e) {

        } finally {
            msgLock.unlock();
        }
    }

    /**
     * 获取一个 BoringQueueMsg
     *
     * @param queueName queue名
     * @return BoringQueueMsg
     */
    public static BoringQueueMsg getBoringQueueMsg(String queueName) {
        BoringQueueMsg boringQueueMsg = boringQueueMsgMap.get(queueName);
        if (ObjectUtil.isEmpty(boringQueueMsg)) {
            Condition condition = msgLock.newCondition();
            msgLock.lock();
            try {
                msgConditionMap.put(queueName, condition);
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                msgLock.unlock();
                msgConditionMap.remove(queueName);
            }
            boringQueueMsg = boringQueueMsgMap.get(queueName);
        }
        return boringQueueMsg;
    }

    /**
     * 添加一个队列与管道的关系
     *
     * @param queueName
     * @param socketChannel
     */
    public static void addQueueForChannel(String queueName, SocketChannel socketChannel) {
        if (CollUtil.isEmpty(queueForChannels.get(queueName))) {
            synchronized (queueForChannels) {
                if (CollUtil.isEmpty(queueForChannels.get(queueName))) {
                    List<SocketChannel> messageListeners = new ArrayList<>();
                    queueForChannels.put(queueName, messageListeners);
                }
            }
        }

        queueForChannels.get(queueName).add(socketChannel);

        listenerLock.lock();
        try {
            listenerConditionMap.get(queueName).signal();
        } catch (Exception e) {

        } finally {
            listenerLock.unlock();
        }
    }

    /**
     * 根据队列名拿到所有关联的channel
     *
     * @param queueName
     * @return
     */
    public static List<SocketChannel> getChannelsByQueueName(String queueName) {
        List<SocketChannel> channelList = queueForChannels.get(queueName);
        if (CollUtil.isEmpty(channelList)) {
            Condition condition = listenerLock.newCondition();
            listenerLock.lock();
            try {
                listenerConditionMap.put(queueName, condition);
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                listenerLock.unlock();
                listenerConditionMap.remove(queueName);
            }
            channelList = queueForChannels.get(queueName);
        }
        return channelList;
    }

    /**
     * 获取所有的queue队列消息
     *
     * @return
     */
    public static ArrayList<BoringQueueMsg> getBoringQueueMsgList() {
        return boringQueueMsgList;
    }

    /**
     * 在连接关闭时，移除队列管理的channel
     *
     * @param socketChannel
     */
    public static void removerQueueForChannel(SocketChannel socketChannel) {
        queueForChannels.forEach((queue, channel) -> {
            channel.remove(socketChannel);
        });
    }

}
