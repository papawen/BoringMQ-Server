package com.ricky.mq.server.code.handler;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.constant.ServerCommandCode;
import com.ricky.mq.server.code.manager.QueueManager;
import com.ricky.mq.server.code.message.BoringQueueMsg;
import com.ricky.mq.server.code.message.data.BaseData;
import com.ricky.mq.server.code.message.data.client.ClientMsgModelData;
import com.ricky.mq.server.code.message.data.server.ServerMsgModelData;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 队列消息模型处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class QueueModeHandler implements Handler {

    private static ThreadLocalRandom random = ThreadLocalRandom.current();

    private static ExecutorService executorService;

    private static QueueModeHandler queueModeHandler = new QueueModeHandler();

    public static QueueModeHandler getQueueModeHandler() {
        return queueModeHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientMsgModelData msgModelData = JSONUtil.toBean(data, ClientMsgModelData.class);

        BoringQueueMsg boringQueueMsg = BoringQueueMsg.build(msgModelData.getModelMsgName(), msgModelData.getMaxSize());
        QueueManager.addBoringQueueMsg(boringQueueMsg);

        BaseData callbackData = ServerMsgModelData.build().setSuccess(true).setUuid(msgModelData.getUuid());
        callbackMsg(socketChannel, ServerCommandCode.CREATE_QUEUE, callbackData);

        // 回调消息
        ArrayList<BoringQueueMsg> boringQueueMsgList = QueueManager.getBoringQueueMsgList();
        if (ObjectUtil.isNotEmpty(executorService)) {
            executorService.shutdown();
        }
        executorService = Executors.newFixedThreadPool(boringQueueMsgList.size());

        boringQueueMsgList.forEach((queueMsg) -> {
            executorService.execute(() -> {
                while (true) {
                    List<SocketChannel> channelList = QueueManager.getChannelsByQueueName(queueMsg.getQueueName());
                    SocketChannel channel = channelList.get(random.nextInt(channelList.size()));
                    try {
                        callbackMsg(channel, ServerCommandCode.LISTENER_QUEUE_MESSAGE, (BaseData) queueMsg.getMessages().take());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        });
    }

}