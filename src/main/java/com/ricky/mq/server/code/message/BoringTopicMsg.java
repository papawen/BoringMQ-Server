package com.ricky.mq.server.code.message;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 广播消息存储类
 *
 * @author ricky
 * @since 2020.06.18
 */
public class BoringTopicMsg {

    private String topicName;

    private LinkedBlockingQueue<TextMsg> messages;

    private BoringTopicMsg() {
    }

    private BoringTopicMsg(String topicName, int maxSize) {
        this.topicName = topicName;
        this.messages = new LinkedBlockingQueue(maxSize);
    }

    public static BoringTopicMsg build(String queueName, int maxSize) {
        return new BoringTopicMsg(queueName, maxSize);
    }

    public String getTopicName() {
        return topicName;
    }

    public LinkedBlockingQueue<TextMsg> getMessages() {
        return messages;
    }
}
