package com.ricky.mq.server.code.handler;

import com.ricky.mq.server.code.message.data.BaseData;
import com.ricky.mq.server.connection.ServerConnection;

import java.nio.channels.SocketChannel;

/**
 * 处理器接口
 *
 * @author ricky
 * @since 2020.06.18
 */
public interface Handler {

    /**
     * 处理客户端发来的消息
     *
     * @param socketChannel 连接管道
     * @param data          数据
     */
    void handler(SocketChannel socketChannel, String data);

    /**
     * 发送处理好的信息给客户端
     *
     * @param socketChannel 连接管道
     * @param commentCode   命令码
     * @param callbackData  数据
     */
    default void callbackMsg(SocketChannel socketChannel, String commentCode, BaseData callbackData) {
        ServerConnection.getServerConnection().send(socketChannel, commentCode, callbackData);
    }

}
