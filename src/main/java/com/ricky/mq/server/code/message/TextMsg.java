package com.ricky.mq.server.code.message;

import com.ricky.mq.server.code.message.data.BaseData;

import java.util.Objects;

/**
 * 最终存储的文本消息
 *
 * @author ricky
 * @since 2020.06.18
 */
public class TextMsg extends BaseData {

    /**
     * 消息唯一标识
     */
    private String uuid;

    /**
     * 消息主题
     */
    private String message;
    /**
     * 消息模型名
     */
    private String modelMsgName;

    private TextMsg() {
    }

    private TextMsg(String uuid, String message, String modelMsgName) {
        this.uuid = uuid;
        this.message = message;
        this.modelMsgName = modelMsgName;
    }

    public static TextMsg build(String uuid, String msg, String modelMsgName) {
        return new TextMsg(uuid, msg, modelMsgName);
    }

    @Override
    public String getUuid() {
        return uuid;
    }


    public String getMessage() {
        return message;
    }

    public String getModelMsgName() {
        return modelMsgName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TextMsg textMsg = (TextMsg) o;
        return Objects.equals(uuid, textMsg.uuid) &&
                Objects.equals(message, textMsg.message) &&
                Objects.equals(modelMsgName, textMsg.modelMsgName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, message, modelMsgName);
    }
}
