package com.ricky.mq.server.code.handler;

import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.manager.TopicManager;
import com.ricky.mq.server.code.message.data.client.ClientMessageListenerData;

import java.nio.channels.SocketChannel;

/**
 * topic监听注册处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ListenerTopicMessageHandler implements Handler {

    private static ListenerTopicMessageHandler listenerQueueMessageHandler = new ListenerTopicMessageHandler();

    public static ListenerTopicMessageHandler getListenerQueueMessageHandler() {
        return listenerQueueMessageHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientMessageListenerData clientMessageListenerData = JSONUtil.toBean(data, ClientMessageListenerData.class);
        TopicManager.addTopicForChannel(clientMessageListenerData.getMsgModelName(), socketChannel);
    }
}
