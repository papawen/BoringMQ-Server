package com.ricky.mq.server.code.message.data.client;

import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 客户端心跳检查
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ClientHeartbeatCheckData extends BaseData {

    public static ClientHeartbeatCheckData build() {
        return new ClientHeartbeatCheckData();
    }

}
