package com.ricky.mq.server.code.handler;

import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.constant.ServerCommandCode;
import com.ricky.mq.server.code.manager.QueueManager;
import com.ricky.mq.server.code.message.BoringQueueMsg;
import com.ricky.mq.server.code.message.TextMsg;
import com.ricky.mq.server.code.message.data.BaseData;
import com.ricky.mq.server.code.message.data.client.ClientBoringMessageData;
import com.ricky.mq.server.code.message.data.server.ServerBoringMessageData;

import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 队列消息处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class QueueMsgHandler implements Handler {


    private static QueueMsgHandler queueModeHandler = new QueueMsgHandler();

    public static QueueMsgHandler getQueueMsgHandler() {
        return queueModeHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientBoringMessageData messageData = JSONUtil.toBean(data, ClientBoringMessageData.class);
        boolean success = false;

        /**
         * 1、获取消息存储类
         * 2、获取实际存储消息的队列
         * 3、判断是否已经存在已保存的消息，不存在则保存
         */
        BoringQueueMsg boringQueueMsg = QueueManager.getBoringQueueMsg(messageData.getModelMsgName());
        LinkedBlockingQueue messages = boringQueueMsg.getMessages();
        TextMsg textMsg = TextMsg.build(messageData.getUuid(), messageData.getBody(), messageData.getModelMsgName());
        if (!messages.contains(textMsg)) {
            if (messages.offer(textMsg)) {
                success = true;
            } else {
                // TODO 没加进去咋办呢？
            }
        }

        BaseData callbackData = ServerBoringMessageData.build().setSuccess(success).setUuid(messageData.getUuid());
        callbackMsg(socketChannel, ServerCommandCode.QUEUE_MESSAGE, callbackData);
    }

}