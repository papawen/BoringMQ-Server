package com.ricky.mq.server.code.manager;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ricky.mq.server.code.message.BoringTopicMsg;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 广播消息管理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class TopicManager {
    /**
     * 监听器锁，为了防止服务器未启动时，客户端先监听队列时找不到队列而报错
     * 加了锁以后可以同步等待创建队列
     */
    private static Lock listenerLock = new ReentrantLock();
    /**
     * 报保存所有监听器锁条件
     */
    private static Map<String, Condition> listenerConditionMap = new ConcurrentHashMap<>(16);
    /**
     * 消息锁，为了防止服务器未启动时，客户端先发送队列消息时找不到队列而报错
     * 加了锁以后可以同步等待创建队列
     */
    private static Lock msgLock = new ReentrantLock();
    /**
     * 报保存所有监听器锁条件
     */
    private static Map<String, Condition> msgConditionMap = new ConcurrentHashMap<>(16);
    /**
     * 保存所有的topic
     */
    private static ArrayList<BoringTopicMsg> boringTopicMsgList = new ArrayList<>(16);
    /**
     * 保存所有的Topic消息
     */
    private static Map<String, BoringTopicMsg> boringTopicMsgMap = new ConcurrentHashMap<>(16);
    /**
     * 保存每个topic所关联的管道
     */
    private static Map<String, CopyOnWriteArrayList<SocketChannel>> topicForChannels = new ConcurrentHashMap<>(16);

    /**
     * 添加一个BoringTopicMsg
     *
     * @param topicMsg 广播消息
     */
    public static void addBoringTopicMsg(BoringTopicMsg topicMsg) {
        if (!boringTopicMsgMap.containsKey(topicMsg.getTopicName())) {
            boringTopicMsgMap.put(topicMsg.getTopicName(), topicMsg);
            boringTopicMsgList.add(topicMsg);
        }

        msgLock.lock();
        try {
            msgConditionMap.get(topicMsg.getTopicName()).signal();
        } catch (Exception e) {

        } finally {
            msgLock.unlock();
        }
    }

    /**
     * 获取一个 BoringTopicMsg
     *
     * @param topicName topic名
     * @return BoringTopicMsg
     */
    public static BoringTopicMsg getBoringTopicMsg(String topicName) {
        BoringTopicMsg boringTopicMsg = boringTopicMsgMap.get(topicName);
        if (ObjectUtil.isEmpty(boringTopicMsg)) {
            Condition condition = msgLock.newCondition();
            msgLock.lock();
            try {
                msgConditionMap.put(topicName, condition);
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                msgLock.unlock();
                msgConditionMap.remove(topicName);
            }
            boringTopicMsg = boringTopicMsgMap.get(topicName);
        }
        return boringTopicMsg;
    }

    /**
     * 添加一个广播与管道的关系
     *
     * @param topicName
     * @param socketChannel
     */
    public static void addTopicForChannel(String topicName, SocketChannel socketChannel) {
        if (CollUtil.isEmpty(topicForChannels.get(topicName))) {
            synchronized (topicForChannels) {
                if (CollUtil.isEmpty(topicForChannels.get(topicName))) {
                    CopyOnWriteArrayList<SocketChannel> messageListeners = new CopyOnWriteArrayList<>();
                    topicForChannels.put(topicName, messageListeners);
                }
            }
        }
        topicForChannels.get(topicName).add(socketChannel);

        listenerLock.lock();
        try {
            listenerConditionMap.get(topicName).signal();
        } catch (Exception e) {

        } finally {
            listenerLock.unlock();
        }
    }

    /**
     * 根据广播名获取所有关联的channel
     *
     * @param topicName
     * @return
     */
    public static List<SocketChannel> getChannelByTopicName(String topicName) {
        List<SocketChannel> channelList = topicForChannels.get(topicName);
        if (CollUtil.isEmpty(channelList)) {
            Condition condition = listenerLock.newCondition();
            listenerLock.lock();
            try {
                listenerConditionMap.put(topicName, condition);
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                listenerLock.unlock();
                listenerConditionMap.remove(topicName);
            }
            channelList = topicForChannels.get(topicName);
        }
        return channelList;
    }

    /**
     * 获取所有的广播消息
     *
     * @return
     */
    public static ArrayList<BoringTopicMsg> getBoringTopicMsgList() {
        return boringTopicMsgList;
    }

    /**
     * 在连接关闭时移除topic关联的channel
     *
     * @param socketChannel
     */
    public static void removerTopicForChannel(SocketChannel socketChannel) {
        topicForChannels.forEach((queue, channel) -> {
            channel.remove(socketChannel);
        });
    }

}
