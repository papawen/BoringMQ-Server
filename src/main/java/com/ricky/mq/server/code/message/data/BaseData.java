package com.ricky.mq.server.code.message.data;

/**
 * 父Data
 *
 * @author ricky
 * @since 2020.06.18
 */
public class BaseData {

    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public BaseData setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }
}
