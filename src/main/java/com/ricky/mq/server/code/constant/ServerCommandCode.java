package com.ricky.mq.server.code.constant;

/**
 * 命令码常量
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ServerCommandCode {
    /**
     * 心跳检查
     */
    public final static String HEART_BEAT_CHECK = "0102";
    /**
     * 创建Queue
     */
    public final static String CREATE_QUEUE = "0104";
    /**
     * 创建Topic
     */
    public final static String CREATE_TOPIC = "0106";
    /**
     * queue消息
     */
    public final static String QUEUE_MESSAGE = "0108";
    /**
     * topic消息
     */
    public final static String TOPIC_MESSAGE = "0110";
    /**
     * 监听queue消息
     */
    public final static String LISTENER_QUEUE_MESSAGE = "0112";
    /**
     * 监听topic消息
     */
    public final static String LISTENER_TOPIC_MESSAGE = "0114";


}
