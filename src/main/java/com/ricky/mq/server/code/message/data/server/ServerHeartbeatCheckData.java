package com.ricky.mq.server.code.message.data.server;

import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 服务端心跳检查
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ServerHeartbeatCheckData extends BaseData {

    private boolean success;

    public static ServerHeartbeatCheckData build() {
        return new ServerHeartbeatCheckData();
    }

    public boolean isSuccess() {
        return success;
    }

    public ServerHeartbeatCheckData setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}
