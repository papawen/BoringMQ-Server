package com.ricky.mq.server.code.handler;

import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.ricky.mq.server.Version;
import com.ricky.mq.server.code.constant.ClientCommandCode;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * 客户端数据处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ClientDataHandler {

    /**
     * 写日志的
     */
    private static Log LOG = LogFactory.get(ClientDataHandler.class);

    /**
     * 处理客户端数据
     *
     * @param clientData
     */
    public static void handler(SocketChannel socketChannel, String clientData) {

        List<String> clientDataList = decomposeMucilage(clientData);
        clientDataList.forEach((x -> {
            if (verification(x)) {
                String commandCode = StrUtil.subWithLength(x, 16, 4);
                String data = new String(HexUtil.decodeHex(StrUtil.subWithLength(x, 20, x.length() - 28)));

                switch (commandCode) {
                    case ClientCommandCode.HEART_BEAT_CHECK:
                        HeartBeatCheckHandler.getHeartBeatCheckHandler().handler(socketChannel, data);
                        break;
                    case ClientCommandCode.CREATE_QUEUE:
                        QueueModeHandler.getQueueModeHandler().handler(socketChannel, data);
                        break;
                    case ClientCommandCode.CREATE_TOPIC:
                        TopicModeHandler.getTopicModeHandler().handler(socketChannel, data);
                        break;
                    case ClientCommandCode.QUEUE_MESSAGE:
                        QueueMsgHandler.getQueueMsgHandler().handler(socketChannel, data);
                        break;
                    case ClientCommandCode.TOPIC_MESSAGE:
                        TopicMsgHandler.getTopicMsgHandler().handler(socketChannel, data);
                        break;
                    case ClientCommandCode.LISTENER_QUEUE_MESSAGE:
                        ListenerQueueMessageHandler.getListenerQueueMessageHandler().handler(socketChannel, data);
                        break;
                    case ClientCommandCode.LISTENER_TOPIC_MESSAGE:
                        ListenerTopicMessageHandler.getListenerQueueMessageHandler().handler(socketChannel, data);
                        break;
                    default:
                        throw new RuntimeException("无此命令：" + commandCode);
                }

            } else {
                LOG.error("clientData：" + x);
            }
        }));
    }


    /**
     * 效验客户端是否合法
     *
     * @param clientData 客户端数据
     * @return
     */
    private static boolean verification(String clientData) {
        if (!StrUtil.startWith(clientData, "FADA", true)) {
            LOG.error("clientData数据不合法！报文不以FADA开头");
            return false;
        }
        if (!StrUtil.endWith(clientData, "ADAF", true)) {
            LOG.error("clientData数据不合法！报文不以ADAF结尾");
            return false;
        }

        int clientDataLength = Integer.parseInt(StrUtil.subWithLength(clientData, 4, 8), 16);
        if (clientDataLength != clientData.length()) {
            LOG.error("clientData数据长度不合法！报文长度域中长度：" + clientDataLength + ",实际长度" + clientData.length());
            return false;
        }

        String version = StrUtil.subWithLength(clientData, 12, 4);
        if (!Version.MESSAGE_VERSION.equalsIgnoreCase(version)) {
            LOG.error("客户端与服务器版本不一致！客户端版本:" + version + ",服务器版本：" + Version.MESSAGE_VERSION);
            return false;
        }

        String clientValidity = StrUtil.subWithLength(clientData, clientData.length() - 8, 4);
        String validityData = StrUtil.subWithLength(clientData, 0, clientData.length() - 8);
        int sum = 0;
        for (int i = 0; i < validityData.length(); i += 2) {
            sum += Integer.parseInt(validityData.substring(i, i + 2), 16);
        }
        sum &= 0xff;
        String validity = StrUtil.padPre(Integer.toHexString(sum), 4, "0");
        if (!clientValidity.equalsIgnoreCase(validity)) {
            LOG.error("clientData数据不合法！效验域验证不一致！");
            return false;
        }

        return true;
    }

    /**
     * 解决黏包问题
     *
     * @param clientData 数据
     * @return 分解好的数据包
     */
    public static List<String> decomposeMucilage(String clientData) {
        List<String> dataList = new ArrayList<>(8);
        do {
            int limit = Integer.parseInt(StrUtil.subWithLength(clientData, 4, 8), 16);
            String left = clientData.substring(0, limit);
            dataList.add(left);
            clientData = clientData.substring(limit);
        } while (clientData.length() > 0);
        return dataList;
    }

}
