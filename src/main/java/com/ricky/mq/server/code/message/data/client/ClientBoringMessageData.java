package com.ricky.mq.server.code.message.data.client;

import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 客户端和服务端之间发送的消息数据体
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ClientBoringMessageData extends BaseData {
    /**
     * 主数据
     */
    private String body;

    /**
     * 消息模型名
     */
    private String modelMsgName;

    public static ClientBoringMessageData build() {
        return new ClientBoringMessageData();
    }

    public String getBody() {
        return body;
    }

    public ClientBoringMessageData setBody(String body) {
        this.body = body;
        return this;
    }

    public String getModelMsgName() {
        return modelMsgName;
    }

    public ClientBoringMessageData setModelMsgName(String modelMsgName) {
        this.modelMsgName = modelMsgName;
        return this;
    }
}
