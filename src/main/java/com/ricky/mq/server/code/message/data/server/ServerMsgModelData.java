package com.ricky.mq.server.code.message.data.server;

import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 服务端消息模型对象
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ServerMsgModelData extends BaseData {

    private boolean success;

    public static ServerMsgModelData build() {
        return new ServerMsgModelData();
    }

    public boolean isSuccess() {
        return success;
    }

    public ServerMsgModelData setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}
