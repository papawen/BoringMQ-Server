package com.ricky.mq.server.code.message.data.server;

import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 客户端和服务端之间发送的消息数据体
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ServerBoringMessageData extends BaseData {

    private boolean success;

    public static ServerBoringMessageData build() {
        return new ServerBoringMessageData();
    }

    public boolean isSuccess() {
        return success;
    }

    public ServerBoringMessageData setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}
