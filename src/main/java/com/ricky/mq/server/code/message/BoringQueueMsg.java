package com.ricky.mq.server.code.message;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 队列消息存储类
 *
 * @author ricky
 * @since 2020.06.18
 */
public class BoringQueueMsg {

    private String queueName;

    private LinkedBlockingQueue<TextMsg> messages;

    private BoringQueueMsg() {

    }

    private BoringQueueMsg(String queueName, int maxSize) {
        this.queueName = queueName;
        this.messages = new LinkedBlockingQueue(maxSize);
    }

    public static BoringQueueMsg build(String queueName, int maxSize) {
        return new BoringQueueMsg(queueName, maxSize);
    }

    public String getQueueName() {
        return queueName;
    }

    public LinkedBlockingQueue getMessages() {
        return messages;
    }
}
