package com.ricky.mq.server.code.handler;

import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.constant.ServerCommandCode;
import com.ricky.mq.server.code.message.data.BaseData;
import com.ricky.mq.server.code.message.data.client.ClientHeartbeatCheckData;
import com.ricky.mq.server.code.message.data.server.ServerHeartbeatCheckData;

import java.nio.channels.SocketChannel;

/**
 * 心跳处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class HeartBeatCheckHandler implements Handler {

    private static HeartBeatCheckHandler heartBeatCheckHandler = new HeartBeatCheckHandler();

    public static HeartBeatCheckHandler getHeartBeatCheckHandler() {
        return heartBeatCheckHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientHeartbeatCheckData checkData = JSONUtil.toBean(data, ClientHeartbeatCheckData.class);
        BaseData callbackData = ServerHeartbeatCheckData.build().setSuccess(true).setUuid(checkData.getUuid());
        callbackMsg(socketChannel, ServerCommandCode.HEART_BEAT_CHECK, callbackData);
    }

}
