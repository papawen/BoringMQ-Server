package com.ricky.mq.server.code.handler;

import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.manager.QueueManager;
import com.ricky.mq.server.code.message.data.client.ClientMessageListenerData;

import java.nio.channels.SocketChannel;

/**
 * queue消息监听注册处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ListenerQueueMessageHandler implements Handler {

    private static ListenerQueueMessageHandler listenerQueueMessageHandler = new ListenerQueueMessageHandler();

    public static ListenerQueueMessageHandler getListenerQueueMessageHandler() {
        return listenerQueueMessageHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientMessageListenerData clientMessageListenerData = JSONUtil.toBean(data, ClientMessageListenerData.class);
        QueueManager.addQueueForChannel(clientMessageListenerData.getMsgModelName(), socketChannel);
    }
}
