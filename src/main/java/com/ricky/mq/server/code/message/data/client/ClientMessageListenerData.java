package com.ricky.mq.server.code.message.data.client;


import com.ricky.mq.server.code.message.data.BaseData;

/**
 * 消息监听
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ClientMessageListenerData extends BaseData {

    private String msgModelName;

    public static ClientMessageListenerData build() {
        return new ClientMessageListenerData();
    }

    public String getMsgModelName() {
        return msgModelName;
    }

    public ClientMessageListenerData setMsgModelName(String msgModelName) {
        this.msgModelName = msgModelName;
        return this;
    }
}
