package com.ricky.mq.server.code.handler;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.constant.ServerCommandCode;
import com.ricky.mq.server.code.manager.TopicManager;
import com.ricky.mq.server.code.message.BoringTopicMsg;
import com.ricky.mq.server.code.message.TextMsg;
import com.ricky.mq.server.code.message.data.BaseData;
import com.ricky.mq.server.code.message.data.client.ClientMsgModelData;
import com.ricky.mq.server.code.message.data.server.ServerMsgModelData;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 广播消息模型处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class TopicModeHandler implements Handler {

    private static ExecutorService executorService;

    private static TopicModeHandler topicModeHandler = new TopicModeHandler();

    public static TopicModeHandler getTopicModeHandler() {
        return topicModeHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientMsgModelData msgModelData = JSONUtil.toBean(data, ClientMsgModelData.class);

        BoringTopicMsg boringTopicMsg = BoringTopicMsg.build(msgModelData.getModelMsgName(), msgModelData.getMaxSize());
        TopicManager.addBoringTopicMsg(boringTopicMsg);

        BaseData callbackData = ServerMsgModelData.build().setSuccess(true).setUuid(msgModelData.getUuid());
        callbackMsg(socketChannel, ServerCommandCode.CREATE_TOPIC, callbackData);

        // 回调消息
        ArrayList<BoringTopicMsg> boringTopicMsgList = TopicManager.getBoringTopicMsgList();
        if (ObjectUtil.isNotEmpty(executorService)) {
            executorService.shutdown();
        }
        executorService = Executors.newFixedThreadPool(boringTopicMsgList.size());

        boringTopicMsgList.forEach((topicMsg) -> {
            executorService.execute(() -> {
                while (true) {
                    try {
                        TextMsg textMsg = topicMsg.getMessages().take();
                        List<SocketChannel> callbackChannels = TopicManager.getChannelByTopicName(topicMsg.getTopicName());
                        callbackChannels.forEach((channel) -> {
                            callbackMsg(channel, ServerCommandCode.LISTENER_TOPIC_MESSAGE, textMsg);
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        });
    }

}