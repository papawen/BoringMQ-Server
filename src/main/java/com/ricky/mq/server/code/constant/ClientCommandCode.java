package com.ricky.mq.server.code.constant;

/**
 * 命令码常量
 *
 * @author ricky
 * @since 2020.06.18
 */
public class ClientCommandCode {

    /**
     * 心跳检查
     */
    public final static String HEART_BEAT_CHECK = "0101";
    /**
     * 创建Queue
     */
    public final static String CREATE_QUEUE = "0103";
    /**
     * 创建Topic
     */
    public final static String CREATE_TOPIC = "0105";
    /**
     * queue消息
     */
    public final static String QUEUE_MESSAGE = "0107";
    /**
     * topic消息
     */
    public final static String TOPIC_MESSAGE = "0109";
    /**
     * 监听queue消息
     */
    public final static String LISTENER_QUEUE_MESSAGE = "0111";
    /**
     * 监听topic消息
     */
    public final static String LISTENER_TOPIC_MESSAGE = "0113";
}
