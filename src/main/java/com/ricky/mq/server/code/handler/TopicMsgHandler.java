package com.ricky.mq.server.code.handler;

import cn.hutool.json.JSONUtil;
import com.ricky.mq.server.code.constant.ServerCommandCode;
import com.ricky.mq.server.code.manager.TopicManager;
import com.ricky.mq.server.code.message.BoringTopicMsg;
import com.ricky.mq.server.code.message.TextMsg;
import com.ricky.mq.server.code.message.data.BaseData;
import com.ricky.mq.server.code.message.data.client.ClientBoringMessageData;
import com.ricky.mq.server.code.message.data.server.ServerBoringMessageData;

import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 队列消息处理器
 *
 * @author ricky
 * @since 2020.06.18
 */
public class TopicMsgHandler implements Handler {

    private static TopicMsgHandler topicMsgHandler = new TopicMsgHandler();

    public static TopicMsgHandler getTopicMsgHandler() {
        return topicMsgHandler;
    }

    @Override
    public void handler(SocketChannel socketChannel, String data) {
        ClientBoringMessageData messageData = JSONUtil.toBean(data, ClientBoringMessageData.class);
        /**
         * 1、获取消息存储类
         * 2、获取实际存储消息的队列
         * 3、判断是否已经存在已保存的消息，不存在则保存
         */
        BoringTopicMsg boringTopicMsg = TopicManager.getBoringTopicMsg(messageData.getModelMsgName());
        LinkedBlockingQueue<TextMsg> messages = boringTopicMsg.getMessages();
        TextMsg textMsg = TextMsg.build(messageData.getUuid(), messageData.getBody(), messageData.getModelMsgName());
        if (!messages.contains(textMsg)) {
            messages.offer(textMsg);
            BaseData callbackData = ServerBoringMessageData.build().setSuccess(true).setUuid(messageData.getUuid());
            callbackMsg(socketChannel, ServerCommandCode.TOPIC_MESSAGE, callbackData);
        }


    }

}