package com.ricky;

import com.ricky.mq.server.connection.ServerConnection;

/**
 * 测试类
 *
 * @author ricky
 * @since 2020.06.18
 */
public class Test {

    public static void main(String[] args) {
        ServerConnection.getServerConnection()
                .port(2428)
                .readBufSize(0xffff)
                .start();
    }

}
